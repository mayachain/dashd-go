integration
===========

[![Build Status](https://gitlab.com/mayachain/dashd-go/workflows/Build%20and%20Test/badge.svg)](https://github.com/btcsuite/btcd/actions)
[![ISC License](http://img.shields.io/badge/license-ISC-blue.svg)](http://copyfree.org)

This contains integration tests which make use of the
[rpctest](https://github.com/btcsuite/btcd/tree/master/integration/rpctest)
package to programmatically drive nodes via RPC.

## License

This code is licensed under the [copyfree](http://copyfree.org) ISC License.
